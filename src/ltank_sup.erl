-module(ltank_sup).

-behaviour(supervisor).

-include("ltank.hrl").

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

%% ===================================================================
%% API functions
%% ===================================================================

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

%% ===================================================================
%% Supervisor callbacks
%% ===================================================================

init([]) ->
    Pubsub = ?CHILD(ltank_pubsubhub, worker),
    StatsSender = ?CHILD(ltank_stats_sender, worker),
    Http = ?CHILD(ltank_http, worker),
    ErrMon = ?CHILD(ltank_error_handler_mon, worker),
    Etop = ?CHILD(ltank_etop, worker),
    AppMon = ?CHILD(ltank_appmon, worker),

    Specs = [Pubsub, StatsSender, AppMon, ErrMon, Etop, Http],

    {ok, { {one_for_one, 5, 10}, Specs} }.

