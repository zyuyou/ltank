%%%-------------------------------------------------------------------
%%% @author zyuyou yuyouchow@gmail.com
%%% @copyright (C) 2015, <COMPANY>
%%% @doc created from bigwig_http
%%%
%%% @end
%%% Created : 16. Jan 2015 下午1:51
%%%-------------------------------------------------------------------
-module(ltank_http).

-behaviour(gen_server).

-include("ltank.hrl").

%% API
-export([start_link/0]).

%% gen_server callbacks
-export([init/1,
	handle_call/3,
	handle_cast/2,
	handle_info/2,
	terminate/2,
	code_change/3]).

-define(SERVER, ?MODULE).

-record(state, {}).

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%%
%% @end
%%--------------------------------------------------------------------
start_link() ->
	gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%%
%% @spec init(Args) -> {ok, State} |
%%                     {ok, State, Timeout} |
%%                     ignore |
%%                     {stop, Reason}
%% @end
%%--------------------------------------------------------------------
init([]) ->
	Port = lib_app:get_env(port, 10120),
	Ip = lib_app:get_env(ip, "127.0.0.1"),
	NumAcceptors = lib_app:get_env(num_acceptors, 16),

	IpStr =
		case is_list(Ip) of
			true ->
				Ip;
			false ->
				inet_parse:ntoa(Ip)
		end,

	?INFO("ltank listening on http://~s:~B/~n", [IpStr, Port]),

	cowboy:start_http(ltank_http_listener, NumAcceptors, [{port, Port}], [{env, [{dispatch, dispatch_rules()}]}]),

	{ok, #state{}}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages
%%
%% @end
%%--------------------------------------------------------------------
handle_call(_Request, _From, State) ->
	{reply, ok, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages
%%
%% @end
%%--------------------------------------------------------------------
handle_cast(_Request, State) ->
	{noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%%
%% @spec handle_info(Info, State) -> {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_info(_Info, State) ->
	{noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%%
%% @spec terminate(Reason, State) -> void()
%% @end
%%--------------------------------------------------------------------
terminate(_Reason, _State) ->
	ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, State, Extra) -> {ok, NewState}
%% @end
%%--------------------------------------------------------------------
code_change(_OldVsn, State, _Extra) ->
	{ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

dispatch_rules() ->
	%% {Host, list({Path, Handler, Opts})}
	cowboy_router:compile(
		[{'_', [
			{"/", cowboy_static, {priv_file, ltank, "static/html/index.html"}},
			{"/index2", cowboy_static, {priv_file, ltank, "static/html/index2.html"}},
			{"/static/[...]", cowboy_static, {priv_dir, ltank, "static/",  [{mimetypes, cow_mimetypes, all}]}},
			{"/vm", ltank_http_vm, []},
			{"/rb/stream", ltank_http_rb_stream, []},
			{"/rb/[...]", ltank_http_rb, []},
			{"/pid/[...]", ltank_http_pid, []},
			{"/module/[...]", ltank_http_module, []},
			{"/top/[...]", ltank_http_etop, []},
			{"/appmon/[...]", ltank_http_appmon, []},
			{"/stats-stream", ltank_http_stats_stream, []},

			{'_', ltank_http_catchall, []}
		]}]
	).

