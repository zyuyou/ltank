%%%-------------------------------------------------------------------
%%% @author jazon
%%% @copyright (C) 2014, <COMPANY>
%%% @doc
%%%    数学计算 类
%%% @end
%%% Created : 22. 五月 2014 下午4:10
%%%-------------------------------------------------------------------
-module(lib_math).

%% API
-export([round_up/1]).

%% @doc 向上取整
round_up(Num) ->
	case Num > trunc(Num) of
		true ->
			trunc(Num) + 1;
		false ->
			trunc(Num)
	end.