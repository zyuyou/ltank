%%%-------------------------------------------------------------------
%%% @author zyuyou yuyouchow@gmail.com
%%% @copyright (C) 2014, Y.S.
%%% @doc
%%%
%%% @end
%%% Created : 24. Mar 2014 8:57 PM
%%%-------------------------------------------------------------------
-module(lib_util).
-author("zyuyou").

-include("lib.hrl").

-include_lib("eunit/include/eunit.hrl").

%% API
-export([md5_string/1]).

-export([atom_to_binary/1, binary_to_atom/1]).
-export([any_to_list/1, any_to_binary/1, any_to_iodata/1]).
-export([term_to_string/1, string_to_term/1]).
-export([decode_term/1, encode_term/1]).
-export([to_integer/1]).

-export([url_decode/1]).

%% @doc 生成md5 16进制字符串
md5_string(Bin) ->
	<<N:128>> = erlang:md5(Bin),
	lists:flatten(io_lib:format("~32.16.0b", [N])).

%%---------------------------------------------------
%%  数据类型转换
%%---------------------------------------------------
%% @doc convert atom to binary
-spec atom_to_binary(Atom :: atom()) -> binary().
atom_to_binary(Atom) ->
	<<131, 100, Len:2/unit:8, Bin:Len/binary>> = term_to_binary(Atom),
	Bin.

%% @doc convert binary to atom
-spec binary_to_atom(Bin :: binary()) -> atom().
binary_to_atom(Bin) ->
	Len = byte_size(Bin),
	binary_to_term(<<131, 100, Len:2/unit:8, Bin/binary>>).

%% @doc any convert to list
-spec any_to_list(Any :: any()) -> list().
any_to_list(Any) when is_list(Any) ->
	Any;
any_to_list(Any) when is_pid(Any) ->
	erlang:pid_to_list(Any);
any_to_list(Any) when is_port(Any) ->
	erlang:port_to_list(Any);
any_to_list(Any) when is_binary(Any) ->
	binary_to_list(Any);
any_to_list(Any) when is_atom(Any) ->
	atom_to_list(Any);
any_to_list(Any) when is_integer(Any) ->
	integer_to_list(Any);
any_to_list(Any) when is_float(Any) ->
	float_to_list(Any);
any_to_list(Any) ->
	term_to_string(Any).

%% @doc convert any to binary
-spec any_to_binary(Any :: any()) -> binary().
any_to_binary(Any) when is_binary(Any) ->
	Any;
any_to_binary(Any) ->
	list_to_binary(any_to_list(Any)).

%% @doc 转化成iodata
any_to_iodata(Any) when is_list(Any) ->
	Any;
any_to_iodata(Any) when is_binary(Any) ->
	Any;
any_to_iodata(Any) when is_atom(Any) ->
	atom_to_list(Any);
any_to_iodata(Any) when is_integer(Any) ->
	integer_to_list(Any);
any_to_iodata(Any) when is_float(Any) ->
	float_to_list(Any).

%% @doc 数字类型转换integer
to_integer(X) when X =:= undefined ->
	0;
to_integer(X) when is_integer(X) ->
	X;
to_integer(X) when is_list(X) ->
	list_to_integer(X);
to_integer(X) when is_binary(X) ->
	to_integer(binary_to_list(X)).

%% @doc 将term转化为string
%% 如[1, 2]转化为"[1,2]"
term_to_string(Term) ->
	lists:flatten(io_lib:format("~w", [Term])).

%% 将string转化为term
%% 如"[1,2]"转化成[1,2]
string_to_term(Str) when is_binary(Str) ->
	string_to_term(?B2S(Str));
string_to_term(Str) when is_list(Str) ->
	case erl_scan:string(Str ++ ".") of
		{error, _, _} = Error ->
			Error;
		{ok, Tokens, _} ->
			erl_parse:parse_term(Tokens)
	end.

%% @doc binary为term
decode_term(Bin) when is_binary(Bin) ->
	{ok, Term} = string_to_term(?B2S(Bin)),
	Term.

%% @doc term为binary
encode_term(Term) ->
	?S2B(term_to_string(Term)).

%% the following url encoding stuff was taken from yaws.erl
%% sadly, cowboy _req:qs_val doesn't url-decode vals
%% http://drproxy.googlecode.com/svn/trunk/extlib/yaws-1.68/src/yaws.erl
-spec url_decode(binary() | list()) -> list().
url_decode(Bin) when is_binary(Bin) ->
	url_decode(binary_to_list(Bin));
url_decode([$%, Hi, Lo | Tail]) ->
	Hex = erlang:list_to_integer([Hi, Lo], 16),
	[Hex | url_decode(Tail)];
url_decode([$? | T]) ->
	[$? | T];
url_decode([H | T]) when is_integer(H) ->
	[H | url_decode(T)];
url_decode([]) ->
	[];
%% deep lists
url_decode([H | T]) when is_list(H) ->
	[url_decode(H) | url_decode(T)].
