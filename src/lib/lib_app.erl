%%%-------------------------------------------------------------------
%%% @author zyuyou yuyouchow@gmail.com
%%% @copyright (C) 2014, Y.S.
%%% @doc 处理app基础函数库
%%%
%%% @end
%%% Created : 25. Mar 2014 2:26 PM
%%%-------------------------------------------------------------------
-module(lib_app).
-author("zyuyou").

%% API
-export([ensure_started/1, ensure_stopped/1]).
-export([is_running/1]).
-export([handle_start_app/1]).
-export([get_env/3, get_env/2]).

%% @spec ensure_started(App::atom()) -> ok
%% @doc Start the given App if it has not been started already.
ensure_started(App) ->
    case application:start(App) of
        ok ->
            ok;
        {error, {already_started, App}} ->
            ok
    end.

%% @spec ensure_started(App::atom()) -> ok
%% @doc Stop the given App if it has not been stopped already.
ensure_stopped(App) ->
    case application:stop(App) of
        ok ->
            ok;
        {error, {already_started, App}} ->
            ok
    end.

%% @doc 处理app启动结果
handle_start_app(ok) -> ok;
handle_start_app({error, {already_started, _}}) -> ok;
handle_start_app(Other) -> throw(Other).

%% @doc 某个app是否运行中
is_running(Apps) when is_list(Apps) ->
    lists:all(fun is_running/1, Apps);
is_running(App) when is_atom(App) ->
    case lists:keyfind(App, 1, application:which_applications()) of
        false ->
            false;
        _ ->
            true
    end.

%% @doc 获取app环境变量
get_env(App, Key, Default) ->
    case application:get_env(App, Key) of
        {ok, Val} ->
            Val;
        undefined ->
            Default
    end.

get_env(Key, Default) ->
    case application:get_env(Key) of
        {ok, Val} ->
            Val;
        undefined ->
            Default
    end.



