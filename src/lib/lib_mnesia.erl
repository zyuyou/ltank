%%%-------------------------------------------------------------------
%%% @author zyuyou yuyouchow@gmail.com
%%% @copyright (C) 2014, Y.S.
%%% @doc 游戏mnesia操作api库
%%%
%%% @end
%%% Created : 16. 三月 2014 下午3:11
%%%-------------------------------------------------------------------
-module(lib_mnesia).
-author("zyuyou").

-include("common.hrl").

%% API
-export([ensure_is_running/0, ensure_not_running/0]).
-export([is_table_exist/1, is_table_empty/1]).
-export([tab_size/1, wild_pattern/1]).
-export([dirty_read/2, dirty_tab2list/1]).
-export([dirty_write/3]).
-export([clear/1]).
-export([connect_nodes/1, add_ram_copy/1, del_ram_copy/1]).

%% @doc ensure mnesia not running
-spec ensure_not_running() -> 'ok'.
ensure_not_running() ->
	lib_app:ensure_stopped(mnesia).

%% @doc ensure mnesia is running
-spec ensure_is_running() -> 'ok'.
ensure_is_running() ->
	lib_app:ensure_started(mnesia).

%%-------------------------------------
%% 表信息相关
%%-------------------------------------
%% @doc 判断表是否存在
is_table_exist(Table) ->
    lists:member(Table, mnesia:system_info(tables)).

%% @doc 判断表是否为空
is_table_empty(Tab) ->
    mnesia:dirty_first(Tab) == '$end_of_table'.

%% @doc 获取表格的wild pattern
wild_pattern(Tab) ->
    mnesia:table_info(Tab, wild_pattern).

%% @doc 表的size(只能是本地表)
tab_size(Tab) ->
    mnesia:table_info(Tab, size).

%%---------------------------------------
%% 读操作相关
%%---------------------------------------
%% @doc 根据指定key, 读取表数据
dirty_read(Table, Key) ->
    case catch mnesia:dirty_read(Table, Key) of
        [{Table, _, Val}] ->
            Val;
        [] ->
            ?NONE;
        {'EXIT', {aborted, _Reason}} ->
%%             ?ERROR("Get Table[~p] Data Error, Reason:~p", [Table, _Reason]),
            ?NONE
    end.

%% @doc tab转化list
dirty_tab2list(Table) ->
    Pattern = wild_pattern(Table),
    case catch mnesia:dirty_match_object(Table, Pattern) of
        {'EXIT', {aborted, _Reason}} ->
%%             ?DEBUG(?F("模式~w匹配对象出错:~w", [Pattern, _Reason])),
            {error, _Reason};
        List ->
            List
    end.

%%---------------------------------------
%% 写操作相关
%%---------------------------------------
%% @doc 根据指定参数写数据表
dirty_write(Table, Key, Val) ->
    case catch mnesia:dirty_write({Table, Key, Val}) of
        ok ->
            ok;
        {'EXIT', {aborted, _Reason}} ->
%%             ?ERROR("Write Table[~p] Data Error, Reason:~p", [Table, _Reason]),
            {error, _Reason}
    end.


%% @doc 清理表中所有数据
clear(Table) ->
	case catch mnesia:clear_table(Table) of
		{atomic, ok} ->
			ok;
		{aborted, _Reason} ->
%% 			?ERROR(?_U("清理表:~p出错:~p"), [Table, _Reason]),
			{error, _Reason}
	end.

%%---------------------------------------
%% 节点之间的操作相关
%%---------------------------------------
%% @doc 连接mnesia nodes
connect_nodes(Nodes) ->
	%?ERROR(?F("mnesia nodes connect_nodes:~p", [Nodes])),
    {ok, _} = mnesia:change_config(extra_db_nodes, Nodes),
    ok.

%% @doc 添加ram copy
%% ram_copies, active_replicas
add_ram_copy(Table) ->
    L = mnesia:table_info(Table, ram_copies),
	%?ERROR(?F("mnesia table_info: ~p ram_copies:~p", [Table, L])),
    case lists:member(node(), L) of
        true ->
            ok;
        false ->
            case catch mnesia:add_table_copy(Table, node(), ram_copies) of
                {atomic, ok} ->
                    ok;
                _Other ->
%%                     ?WARN(?F("添加表:~p的ram_copy失败:~p", [Table, _Other])),
                    ok
            end
    end.

%% @doc 删除ram copy
del_ram_copy(Table) ->
	L = mnesia:table_info(Table, ram_copies),
	case lists:member(node(), L) of
		true ->
			case catch mnesia:del_table_copy(Table, nodes()) of
				{atomic, ok} ->
					ok;
				_Other ->
%% 					?WARN(?F("删除表:~p的ram_copy失败:~p", [Table, _Other])),
					ok
			end;
		false ->
			ok
	end.