%%%-------------------------------------------------------------------
%%% @author zyuyou yuyouchow@gmail.com
%%% @copyright (C) 2014, Y.S.
%%% @doc
%%%
%%% @end
%%% Created : 27. Mar 2014 4:13 PM
%%%-------------------------------------------------------------------
-module(lib_lists).

-include("lib.hrl").

%% API
-export([rand_pick/1, rand_pick_n/2, rand_pick_n/3]).
-export([pick_by_weight/1, pick_by_prob/1, pick_by_time/2, pick_by_range/2]).
-export([join/2, get_number_of_list/2]).
-export([strong_rand_pick/1]).

%% @doc random pick one element from the list.
%%     the List must not be empty
-spec rand_pick(List :: [any(), ...]) -> any().
rand_pick([_|_] = List) ->
    Pos = random:uniform(length(List)),
    lists:nth(Pos, List).

strong_rand_pick([_|_] = List) ->
	<<A:32, B:32, C:32>> = crypto:strong_rand_bytes(12),
	random:seed({A, B, C}),
	Pos = random:uniform(length(List)),
	lists:nth(Pos, List).

%% @doc 在某个序列中随机N个值
rand_pick_n(Max, N) when Max < N ->
	lists:seq(1, Max);
rand_pick_n(Max, N) when is_integer(Max) ->
	rand_pick_n(lists:seq(1, Max), N, []);
rand_pick_n(List, N) when length(List) < N ->
	List;
rand_pick_n(List, N) when is_list(List) ->
	rand_pick_n(List, N, []).

rand_pick_n(_DS, N, Result) when length(Result) =:= N ->
	Result;
rand_pick_n(DS, N, Result) ->
	Len = length(DS),
	Rand = lib_num:rand(1, Len),
	Number = lists:nth(Rand, DS),
	rand_pick_n(lists:delete(Number, DS), N, [Number|Result]).

%% @doc 依据权重选择
%% 列表格式:[{对象,权重}]
%% 返回{对象,位置}
pick_by_weight(List) ->
	Sum =
		lists:foldl(
			fun(E, Sum0) ->
				Range1 =
					case E of
						{_Obj, Range} ->
							Range;
						[_Obj, Range] ->
							Range
					end,
				Sum0 + Range1
			end, 0, List),
	Point = lib_num:rand(Sum),
	do_pick_by_prob(List, 0, Point, 1).

%% @doc 依据概率选择
%% 列表格式:[{对象,概率}]
%% 返回{对象,位置}
pick_by_prob(List) ->
	Point = lib_num:rand(),
	do_pick_by_prob(List, 0, Point, 1).

do_pick_by_prob([], _Cur, _Point, _Index)->
	none;
do_pick_by_prob([E | Rest], Cur, Point, Index) ->
	{Obj1, Range1} =
		case E of
			{Obj, Range} ->
				{Obj, Range};
			[Obj, Range] ->
				{Obj, Range}
		end,
	if
		Cur < Point andalso Point =< (Range1 + Cur) ->
			{Obj1, Index};
		true ->
			do_pick_by_prob(Rest, Range1 + Cur, Point, Index + 1)
	end.

%% @doc 依据时间选择数值
%% List: [{Time, Value}],Time从大到小
pick_by_time(List, Cur) ->
	do_pick_by_time(List, Cur).
do_pick_by_time([], _Cur) ->
	0;
do_pick_by_time([{Time, Value} | _T], Cur) when Cur >= Time ->
	Value;
do_pick_by_time([{_Time, _Value} | T], Cur) ->
	do_pick_by_time(T, Cur).

%% @doc 模拟String的Join函数
join([], _Seq) ->
	[];
join([H|T], Seq) ->
	Tail =
		lists:foldr(
			fun(E, Acc) ->
				[Seq, E | Acc]
			end, [], T),
	[H | Tail].

%% @doc 根据T的值在某个区间取相应的值
%% N: Integer or Float
%% List: [{RangeStart, RangeEnd, Value} ...]
pick_by_range(N, List) ->
	do_pick_by_range(N, List).

do_pick_by_range(_N, []) ->
	none;
do_pick_by_range(N, [{RangeStart, RangeEnd, Value} | T]) ->
	if
		N >= RangeStart andalso N =< RangeEnd ->
			Value;
		true ->
			do_pick_by_range(N, T)
	end.


%% @doc 获取一个目标在列表里的序列号
get_number_of_list(Member, List) ->
	{_Num, Nth} =
	lists:foldl(
		fun
			(Member0, {Num, 0}) when Member0 =:= Member ->
				{Num, Num};
			(_Member, {Num, Nth0}) ->
				{Num + 1, Nth0}
			end, {1,0}, List),
	Nth.

