%%%-------------------------------------------------------------------
%%% @author zyuyou yuyouchow@gmail.com
%%% @copyright (C) 2014, Y.S.
%%% @doc 随机 概率 相关函数库
%%%
%%% @end
%%% Created : 24. Mar 2014 10:56 AM
%%%-------------------------------------------------------------------
-module(lib_num).
-author("zyuyou").

-include("common.hrl").

%% API
-export([rand/0, rand/1, rand/2]).
-export([strong_rand/0, strong_rand/1, strong_rand/2]).

%% @doc 返回1-100之间的一个数字
rand() ->
    rand(?PROB_FULL).

%% @doc 返回一个随机数(结果1-N)
rand(0) -> 0;
rand(N) when N > 0 ->
    random:uniform(N).

%% @doc 返回两个数字之间的随机数
rand(Min, Min) ->
    Min;
rand(Min, Max) ->
    rand(Max - Min + 1) + Min - 1.

%% @doc 强随机数(重置种子)
strong_rand() ->
	strong_rand(?PROB_FULL).

%% @doc 返回一个强随机数(结果1-N)
strong_rand(0) ->
	0;
strong_rand(N) when N > 0 ->
	<<A:32, B:32, C:32>> = crypto:strong_rand_bytes(12),
	random:seed({A, B, C}),
	random:uniform(N).

%% @doc 返回两个数字之间的强随机数
strong_rand(Min, Min) ->
	Min;
strong_rand(Min, Max) ->
	strong_rand(Max - Min + 1) + Min - 1.
