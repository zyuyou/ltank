%%%-------------------------------------------------------------------
%%% @author zyuyou yuyouchow@gmail.com
%%% @copyright (C) 2014, Y.S.
%%% @doc
%%%
%%% @end
%%% Created : 21. Mar 2014 11:34 AM
%%%-------------------------------------------------------------------
-module(lib_sup).
-author("zyuyou").

-include("common.hrl").

%% API
-export([start_child/2]).

%% 动态创建子进程
start_child(Sup, {_ChildName, _, _, _, _, _} = Child) ->
    case catch supervisor:start_child(Sup, Child) of
        {ok, _Pid} ->
%%             ?INFO("child:~p started! Pid:~p", [ChildName, Pid]),
	        %?ERROR("child:~p started!", [ChildName]),
            ok;
        {error, {{already_started, _Pid}, _}} ->
%%             ?INFO("child:~p already started! Pid:~p", [ChildName, _Pid]),
	        %?ERROR("child:~p already started! Pid:~p", [ChildName, _Pid]),
            ok;
	    {error, {already_started, _Pid}} ->
%% 		    ?INFO("child:~p already started! Pid:~p", [ChildName, _Pid]),
		    %?ERROR("child:~p already started! Pid:~p", [ChildName, _Pid]),
		    ok;
	    {error, {normal, _Reason}} ->
%% 		    ?INFO("child:~p stop normal!, Reason:~p", [ChildName, _Reason]),
		    ok;
	    {error, {ignore, _Reason}} ->
%% 		    ?INFO("child:~p stop ignore!, Reason:~p", [ChildName, _Reason]),
		    ok;
        Other ->
%%             ?ERROR("start child service:~p fail, Reason:~p", [ChildName, Other]),
            throw(Other)
    end.
