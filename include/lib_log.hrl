-ifndef(GAMELIB_LOG_HRL).
-define(GAMELIB_LOG_HRL, true).

%% live方式启动在屏幕输出
-ifdef(LIVE).
-define(_U(Text), (unicode:characters_to_list(list_to_binary(Text)))).
-else.
-define(_U(Text), Text). % 输出到文件
-endif.

-define(DEBUG, 128).    % 调试
-define(INFO, 64).      % 信息
-define(NOTICE, 32).    % 通知
-define(WARNING, 16).   % 警告
-define(ERROR, 8).      % 错误
-define(CRITICAL, 4).   % 紧急(关键)
-define(ALERT, 2).      % 警报
-define(EMERGENCY, 1).  % 紧急(突发)
-define(LOG_NONE, 0).

%%%----------------------
%%% @doc 内联代码定义
%%%----------------------
%% @doc Debug 类型日志
-define(DEBUG(F), lager:debug(F)).
-define(DEBUG(F, D), lager:debug(io_lib:format(F, D))).
-define(DEBUG(A, F, D), lager:debug(A, F, D)).

%% @doc Info 类型日志
-define(INFO(F), lager:info(F)).
-define(INFO(F, D), lager:info(io_lib:format(F, D))).
-define(INFO(A, F, D), lager:info(A, F, D)).

%% @doc Warning 类型日志
-define(WARN(F), lager:warning(F)).
-define(WARN(F, D), lager:warning(io_lib:format(F, D))).
-define(WARN(A, F, D), lager:warning(A, F, D)).

%% @doc Notice 类型日志
-define(NOTICE(F), lager:notice(F)).
-define(NOTICE(F, D), lager:notice(io_lib:format(F, D))).
-define(NOTICE(A, F, D), lager:notice(A, F, D)).

%% @doc Error 类型日志
-define(ERROR(F), lager:error(F)).
-define(ERROR(F, D), lager:error(io_lib:format(F, D))).
-define(ERROR(A, F, D), lager:error(A, F, D)).

%% @doc Critical 类型日志
-define(CRITICAL(F), lager:critical(F)).
-define(CRITICAL(F, D), lager:critical(io_lib:format(F, D))).
-define(CRITICAL(A, F, D), lager:critical(A, F, D)).

%% @doc Alert 类型日志
-define(ALERT(F), lager:alert(F)).
-define(ALERT(F, D), lager:alert(io_lib:format(F, D))).
-define(ALERT(A, F, D), lager:alert(A, F, D)).

%% @doc Emergency 类型日志
-define(EMERGENCY(F), lager:emergency(F)).
-define(EMERGENCY(F, D), lager:emergency(io_lib:format(F, D))).
-define(EMERGENCY(A, F, D), lager:emergency(A, F, D)).

-endif. % GAMELIB_LOG_HRL