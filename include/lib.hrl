%%%----------------------------------------------------------------------
%%%
%%% @copyright YouScape, y.s.
%%%
%%% @doc ys header file used by user, 服务器系统的一些基础定义
%%%
%%%----------------------------------------------------------------------
-ifndef(LIB_HRL).
-define(LIB_HRL, true).

%%%----------------------
%%% @doc EUNIT相关定义
%%%----------------------
-ifdef(EUNIT).
-include_lib("eunit/include/eunit.hrl").
-endif.

%% Print in standard output
-define(PRINT, io:format).

%%%----------------------
%%% @doc 程序超时时间相关
%%%----------------------
-define(GEN_TIMEOUT, 6000).

%%%----------------------
%%% @doc 内联代码定义
%%%----------------------
%% syntax similar with ?: in c
-ifndef(IF).
-define(IF(C, T, F), (case (C) of true -> (T); false -> (F) end)).
-endif.

%% 退出
-ifndef(EXIT).
-define(EXIT(C), timer:sleep(100), init:stop(C)).
-endif.

%% 抛出错误码
-define(C2SERR(Code), throw({error, Code})).

%%%----------------------
%%% @doc convert macros
%%%----------------------
-define(B2S(B), (binary_to_list(B))).
-define(S2B(S), (list_to_binary(S))).
-define(C2B(C), unicode:characters_to_binary(C)).
-define(N2S(N), integer_to_list(N)).
-define(S2N(S), list_to_integer(S)).
-define(N2B(N), ?S2B(integer_to_list(N))).
-define(B2N(B), list_to_integer(?B2S(B))).

-define(A2S(A), atom_to_list(A)).
-define(S2A(S), list_to_atom(S)).
-define(S2EA(S), list_to_existing_atom(S)).

-define(ANY2B(Any), lib_util:any_to_binary(Any)).
-define(ANY2S(Any), lib_util:any_to_list(Any)).

-define(INT(X), lib_util:to_integer(X)).

-define(U(Text), unicode:characters_to_list(list_to_binary(Text))).
-define(F(F, D), io_lib:format(F, D)).

%%%----------------------
%%% @doc 断言相关定义
%%%----------------------
%%-ifdef(TEST).
-define(ASSERT(Con), (
	case Con of
		true ->
			ok;
		false ->
			error({assert, ??Con})
	end)).
%%-else.
%%-define(ASSERT(Con), ok).
%%-endif.

%%%----------------------
%%% @doc dialyzer相关定义
%%%----------------------
%%
%% 当进行dialyzer时用来屏蔽ets concurrency_read属性
%%
%%-ifdef(DIALYZER).
%-define(ETS_CONCURRENCY, {write_concurrency, true}).
-define(ETS_CONCURRENCY, {read_concurrency, true}).
%-else.
%-define(ETS_CONCURRENCY, {read_concurrency, true}, {write_concurrency, true}).
%-endif.

%%%----------------------
%%% @doc 常规数值获取
%%%----------------------
%% 关于时间
-define(MINUTE_SEC, 60).   % 一分钟秒数
-define(HOUR_SEC, 3600).   % 一小时秒数
-define(DAY_SEC, 86400).   % 一天秒数
-define(WEEK_SEC, 604800).   % 一周秒数



%%%----------------------
%%% @doc 概率,随机相关参数
%%%----------------------
-define(PROB_FULL, 100).


-endif. % LIB_HRL
