%%%-------------------------------------------------------------------
%%% @author zyuyou
%%% @copyright (C) 2014, <COMPANY>
%%% @doc 常量头文件
%%%
%%% @end
%%% Created : 16. 三月 2014 上午2:00
%%%-------------------------------------------------------------------
-author("zyuyou").

%%%----------------------
%%% @doc 关键词定义
%%%----------------------
-define(NONE, none).
-define(UNDEF, undefined).

%% 开关
-define(SWITCH_CLOSE, 0).       % 关闭
-define(SWITCH_OPEN, 1).        % 开启

%% 奖励状态
-define(REWARD_CAN_NOT_GET, 0).	% 奖励未能领
-define(REWARD_CAN_GET, 1).		% 奖励可领
-define(REWARD_HAS_GET, 2).		% 奖励已领