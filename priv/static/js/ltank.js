/**
 * Created by root on 15-1-22.
 */
LTANK = (function(){
    (function () {
        $('#ltankNav li').click(function(e){
            e.preventDefault();
            $('#ltankNav li.active').removeClass('active');
            $(this).addClass('active');

            $('.tab-content div.tab-pane.fade.active.in').removeClass('active').removeClass('in');
            $($(this).children('a').attr('href')).addClass('active').addClass('in');

            switch ($(this).children('a').attr('href')){
                case "#appmon":
                    APPMON.start('nonode@nohost');
                    break;
                case "#stats":
                    Stats.start();
            }
        });
    })();
})();

$(function () {
    $.getJSON('/vm', function (json) {
        var system_info = json['system_info'];
        for(var i in system_info){
            $('#system-info-table').append($('<tr><td>' + i + '</td><td>' + system_info[i] + '</td></tr>'));
        }

        var releases = json['releases'];
        for(var r in releases){
            $('#releases-table').append($('<tr><td>' + r + '</td><td>' + releases[r].version + '</td><td>' + releases[r].status + '</td><td>' + releases[r].deps + '</td></tr>'));
        }

        for(var k in json['applications']){
            for(var a in json['applications'][k]){
                var app = json['applications'][k][a];
                $('#' + k + '-app-table').append($('<tr><td>' + app.name + '</td><td>' + app.version + '</td><td>' + app.description + '</td></tr>'));
            }
        }
    });

    $("#dialog").dialog({
        title: "dialog title",
        width: 550
    });

});