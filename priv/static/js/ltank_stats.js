/**
 * Created by zyuyou on 15-1-22.
 */
var MAX_POINTS = 50;
var stats_data = {};

var process_stats_option = {
    title : {text: 'Processes', subtext: '进程数'},
    tooltip : {trigger: 'axis'},
    xAxis : [
        {
            type : 'category',
            boundaryGap : true,
            data : (function (){
                var now = new Date();
                var res = [];
                var len = MAX_POINTS;
                while (len--) {
                    res.unshift(now.toLocaleTimeString().replace(/^\D*/,''));
                    now = new Date(now - 2000);
                }
                return res;
            })()
        }
    ],
    yAxis : [
        {
            type : 'value',
            scale: true,
            min: 0,
            splitNumber:10,
            boundaryGap: [0.2, 0.2]
        }
    ]
};

var reductions_stats_option = {
    title : {text:'Reductions', subtext: '调度规约数'},
    tooltip : {trigger: 'axis'},
    xAxis : [
        {
            type : 'category',
            boundaryGap : true,
            data : (function (){
                var now = new Date();
                var res = [];
                var len = MAX_POINTS;
                while (len--) {
                    res.unshift(now.toLocaleTimeString().replace(/^\D*/,''));
                    now = new Date(now - 2000);
                }
                return res;
            })()
        }
    ],
    yAxis : [
        {
            type : 'value',
            scale: true,
            min: 0,
            boundaryGap: [0.2, 0.2]
        }
    ]
};

var bytes_in_stats_option = {
    title : {text: 'BytesIn', subtext: "单位: byte"},
    tooltip : {trigger: 'axis'},
    xAxis : [
        {
            type : 'category',
            boundaryGap : true,
            data : (function (){
                var now = new Date();
                var res = [];
                var len = MAX_POINTS;
                while (len--) {
                    res.unshift(now.toLocaleTimeString().replace(/^\D*/,''));
                    now = new Date(now - 2000);
                }
                return res;
            })()
        }
    ],
    yAxis : [
        {
            type : 'value',
            scale: true,
            min: 0,
            boundaryGap: [0.2, 0.2]
        }
    ]
};

var bytes_out_stats_option = {
    title : {text: 'BytesOut', subtext: "单位: byte"},
    tooltip : {trigger: 'axis'},
    xAxis : [
        {
            type : 'category',
            boundaryGap : true,
            data : (function (){
                var now = new Date();
                var res = [];
                var len = MAX_POINTS;
                while (len--) {
                    res.unshift(now.toLocaleTimeString().replace(/^\D*/,''));
                    now = new Date(now - 2000);
                }
                return res;
            })()
        }
    ],
    yAxis : [
        {
            type : 'value',
            scale: true,
            min: 0,
            splitNumber:10,
            boundaryGap: [0.2, 0.2]
        }
    ]
};

var run_queue_stats_option = {
    title : {text: 'RunQueue', subtext: '运行队列'},
    tooltip : {trigger: 'axis'},
    xAxis : [
        {
            type : 'category',
            boundaryGap : true,
            data : (function (){
                var now = new Date();
                var res = [];
                var len = MAX_POINTS;
                while (len--) {
                    res.unshift(now.toLocaleTimeString().replace(/^\D*/,''));
                    now = new Date(now - 2000);
                }
                return res;
            })()
        }
    ],
    yAxis : [
        {
            type : 'value',
            scale: true,
            min: 0,
            splitNumber:10,
            boundaryGap: [0.2, 0.2]
        }
    ]
};

var stats_option = {
    'process_count': process_stats_option,
    'reductions': reductions_stats_option,
    'bytes_in': bytes_in_stats_option,
    'bytes_out': bytes_out_stats_option,
    'run_queue': run_queue_stats_option
};

var axisData;

Stats = (function(){
    function _start(){
        if(!$('#process_count').data('stats')){
            if (!stats_data['process_count']) {
                for(var key in stats_option){
                    stats_data[key] = [];
                }
            }

            var i = MAX_POINTS - stats_data['process_count'].length ;
            // 若数据不够,填充0
            while(i > 0){
                for(var key in stats_option){
                    stats_data[key].unshift(0);
                }
                i--;
            };

            for(var key in stats_option){
                var charts = echarts.init($('#' + key)[0]);
                charts.setOption(stats_option[key]);
                charts.setSeries([{name: key, type: 'line', data: stats_data[key]}]);
                $('#' + key).data('stats', charts);
            }
        }
    }

    return {
        start: _start
    };
})();

<!-- connect to realtime stats feed -->
$('#stats').bind('onupdate', function (e, data) {
    axisData = (new Date()).toLocaleTimeString().replace(/^\D*/,'');

    for (var key in data) {
        if (key == 'date')   continue;
        if (key == 'uptime') continue;

        if (!stats_data[key]) stats_data[key] = [];

        stats_data[key].push(data[key]);
        stats_data[key] = stats_data[key].slice(-(MAX_POINTS + 1));

        if($('#ltankNav li.active').children('a').attr('href') == "#stats"){
            $('#' + key).data('stats').addData([[0,data[key],false,false,axisData]]);
        }
    }

});

TPL.connect("/stats-stream");
