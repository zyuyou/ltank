#ltank

由于想对服务器的Erlang VM有个可视化的监控, 于是上网搜了一下, 发现只有bigwig(https://github.com/beamspirit/bigwig.git), 但是最后的更新是2012年４月份的, 其所使用的cowboy和jsx都好旧了.

想着顺便提高下自己关于监控Erlang虚拟机方面的知识, 所以就在bigwig的基础上, 将其进行更新, 顺便加入自己需要的东西.

### 目前/index.html保持原来bigwig的版本,　/index2 为本人正在修改的版本

### P.S. 切勿吐槽本人写的前端代码

<s>目前还处于更新cowboy和jsx的版本, 修改代码兼容中, 项目还不可以运行...</s>

<s>2015.01.19 目前修改使用了新版本的cowboy和otp-17关于etop改进了walk_clock和runtime的处理, 不过因为我在项目中加入lager, 原有使用sasl的reports还有点问题, 后续会直接使用lager的.
现在项目是可以运行的啦</s>

<s>2015.01.20 修复error_handler推送时的消息格式错误问题(之前以为是加入了lager的原因, 今天想了一下,　对于这样一个工具项目应该越简洁越好,　所以lager应该只是用于开发时使用);
另外尝试整合进其他项目时,　发现appmon信息没有分节点,　若监控多节点项目时会有问题.　后续再修改下.</s>

<s>2015.01.26 修改Node Stats为专门的标签显示, 并且改为显示当前值而非波动值. 感觉原来仅显示数据波动且显示区域相当小, 实际作用不大.　</s>

<s>2015.01.27 修改原来appmon_info获取进程信息的process_info 为try_process_info（rpc_call实现）, 解决获取非本节点进程信息出错问题.</s>

2015.02.14 移除了reports项(目前对自己的项目来说作用不大), 修复了进程信息弹出框问题, 因为换了新版本的jquery-ui好像不支持$("#Port<0.6588>")这样的转换.

### 工作上事情太多, 再加上本人web前端技术有限, 更新比较慢. 后续的一个目标是做一个web版的erlang的observer app.




